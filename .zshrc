#	         _              
#	 _______| |__  _ __ ___ 
#	|_  / __| '_ \| '__/ __|
#	 / /\__ \ | | | | | (__ 
#	/___|___/_| |_|_|  \___|
		
# oh-my-zsh section 
export ZSH="/home/aashu/.oh-my-zsh"
export ZSH_THEME="spaceship"
export plugins=(z git zsh-syntax-highlighting zsh-autosuggestions zsh-history-substring-search)
source $ZSH/oh-my-zsh.sh

# options section
setopt correct                                                  # Auto correct mistakes
setopt extendedglob                                             # Extended globbing. Allows using regular expressions with *
setopt nocaseglob                                               # Case insensitive globbing
setopt rcexpandparam                                            # Array expension with parameters
setopt nocheckjobs                                              # Don't warn about running processes when exiting
setopt numericglobsort                                          # Sort filenames numerically when it makes sense
setopt nobeep                                                   # No beep
setopt appendhistory                                            # Immediately append history instead of overwriting
setopt histignorealldups                                        # If a new command is a duplicate, remove the older one
setopt autocd                                                   # if only directory path is entered, cd there.

# set zsh-completion options
zstyle ':completion:*' matcher-list 'm:{a-zA-Z}={A-Za-z}'       # Case insensitive tab completion
zstyle ':completion:*' list-colors ${(s.:.)LS_COLORS}           # Colored completion (different colors for dirs/files/etc)
zstyle ':completion:*' rehash true                              # automatically find new executables in path 
zstyle ':completion:*' accept-exact '*(N)'
zstyle ':completion:*' use-cache on
zstyle ':completion:*' cache-path ~/.zsh/cache

HISTFILE=~/.zsh_history
HISTSIZE=20000
SAVEHIST=10000
WORDCHARS=${WORDCHARS//\/[&.;]}                                 # Don't consider certain characters part of the word

# keybindings section
bindkey -e
bindkey '^[[7~' beginning-of-line                               # Home key
bindkey '^[[H' beginning-of-line                                # Home key
if [[ "${terminfo[khome]}" != "" ]]; then
  bindkey "${terminfo[khome]}" beginning-of-line                # [Home] - Go to beginning of line
fi
bindkey '^[[8~' end-of-line                                     # End key
bindkey '^[[F' end-of-line                                      # End key
if [[ "${terminfo[kend]}" != "" ]]; then
  bindkey "${terminfo[kend]}" end-of-line                       # [End] - Go to end of line
fi
bindkey '^[[2~' overwrite-mode                                  # Insert key
bindkey '^[[3~' delete-char                                     # Delete key
bindkey '^[[C'  forward-char                                    # Right key
bindkey '^[[D'  backward-char                                   # Left key
bindkey '^[[5~' history-beginning-search-backward               # Page up key
bindkey '^[[6~' history-beginning-search-forward                # Page down key

# navigate words with ctrl+arrow keys
bindkey '^[Oc' forward-word                                     #
bindkey '^[Od' backward-word                                    #
bindkey '^[[1;5D' backward-word                                 #
bindkey '^[[1;5C' forward-word                                  #
bindkey '^H' backward-kill-word                                 # delete previous word with ctrl+backspace
bindkey '^[[Z' undo                                             # Shift+tab undo last action

# alias section
alias ...='cd ../..'											# Lazy cd ../..
alias ls='ls --color=tty'										# Use tty colors when ls-ing
alias grep='grep --color=auto'									# Use tty colors when grep-ing
alias cp='cp -i'                                                # Confirm before overwriting something
alias df='df -h'                                                # Human-readable sizes
alias free='free -m'                                            # Show sizes in MB
alias i3lock='~/.config/i3lock/lock.sh'							# i3lock script
alias config='/usr/bin/git --git-dir=$HOME/dev/dotfiles --work-tree=$HOME'
alias pfetch='PF_INFO="ascii title os kernel uptime pkgs memory" HOSTNAME="nautilus" pfetch'

# color man pages
export LESS_TERMCAP_mb=$'\E[01;32m'
export LESS_TERMCAP_md=$'\E[01;32m'
export LESS_TERMCAP_me=$'\E[0m'
export LESS_TERMCAP_se=$'\E[0m'
export LESS_TERMCAP_so=$'\E[01;47;34m'
export LESS_TERMCAP_ue=$'\E[0m'
export LESS_TERMCAP_us=$'\E[01;36m'
export LESS=-r

# bind UP and DOWN arrow keys to history substring search
zmodload zsh/terminfo
bindkey "$terminfo[kcuu1]" history-substring-search-up
bindkey "$terminfo[kcud1]" history-substring-search-down
bindkey '^[[A' history-substring-search-up			
bindkey '^[[B' history-substring-search-down

# tell GPG to pipe output to tty
export GPG_TTY=$(tty)

# change prompt at ⏾
#if [[ $(~/Documents/scripts/is-it-dark.sh) -ne 0 ]]; then SPACESHIP_CHAR_SYMBOL="⏾ " fi

# functions
mcd () { mkdir -p "$1" && cd "$1" }

source "/home/aashu/.oh-my-zsh/custom/themes/spaceship.zsh-theme"

#export custom variables
export SUDO_EDITOR=/usr/bin/vim 
export QT_AUTO_SCREEN_SCALE_FACTOR=1.25
export GDK_SCALE=2
export GDK_DPI_SCALE=0.25
alias markdownToPandoc="pandoc -V geometry:margin=1in a.md -o a.pdf"
# functions 
ldu() { command du -ahLd 1 2> /dev/null | sort -rh | head -n 20 ; }
rc(){
	systemctl list-unit-files --type=service |\
	sed 's/.service//g' |\
	sed '/static/d' |\
	sed '/indirect/d' |\
	sed '/systemd/d' |\
	sed '/dbus-org/d' |\
	sed '/canberra/d'|\
	sed '/wpa_supplicant/d' |\
	sed '/netctl/d' |\
	sed '/rfkill/d' |\
	sed '/krb5/d' |\
	tail -n+2 |\
	head -n -2 |\
	sed 's/\(^.*enabled.*$\)/[x] \1/' |\
	sed 's/enabled//g' |\
	sed 's/\(^.*disabled.*$\)/[ ] \1/' |\
	sed 's/disabled//g' |\
	sed 's/[ \t]*$//' |\
	while read line; do
			if [[ $line == *'[x]'* ]]; then
				printf "\033[0;32m$line\n"
			else
				printf "\033[1;30m$line\n"
			fi
	done
}

# env variables
export PATH=$PATH:~/Android/Sdk/tools/:~/Android/Sdk/platform-tools/:/home/aashu/.local/bin
alias ytdl=youtube-dl

# >>> conda initialize >>>
# !! Contents within this block are managed by 'conda init'     !!
 __conda_setup="$('/home/aashu/anaconda3/bin/conda' 'shell.ba    sh' 'hook' 2> /dev/null)"
 if [ $? -eq 0 ]; then
     eval "$__conda_setup"
  else
      if [ -f "/home/aashu/anaconda3/etc/profile.d/conda.sh" ]    ; then
          . "/home/aashu/anaconda3/etc/profile.d/conda.sh"
      else
          export PATH="/home/aashu/anaconda3/bin:$PATH"
      fi
  fi
  unset __conda_setup
  # <<< conda initialize <<<
export PATH="/home/aashu/anaconda3/bin:$PATH"
